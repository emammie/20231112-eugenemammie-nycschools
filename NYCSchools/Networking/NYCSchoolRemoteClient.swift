//
//  NYCSchoolRemoteClient.swift
//  NYCSchools
//
//  Created by  Eugene Mammie on 11/14/23.
//

import Foundation

enum LoadingError: Error {
    case clientLoadingError, parsingError
}

protocol URLSessionProtocol {
    func data(from url: URL) async throws -> (Data, URLResponse)
}

protocol LoadingClient {
    func fetchSchools() async throws -> [School]
}

struct NYCSchoolRemoteClient: LoadingClient {
    static let url = URL(string: "https://data.cityofnewyork.us/resource/97mf-9njv.json")
    static let schoolsURL = URL(string: "https://data.cityofnewyork.us/resource/734v-jeq5.json")

    var session: URLSessionProtocol

    init(aSession: URLSessionProtocol = URLSession.shared) {
        session = aSession
    }

    func fetchSchools() async throws -> [School] {
        let (data, response) = try await session.data(from: NYCSchoolRemoteClient.schoolsURL!)
        guard let httpResponse = response as? HTTPURLResponse,
              httpResponse.statusCode == 200
        else {
            throw LoadingError.clientLoadingError
        }

        let schools = try JSONDecoder().decode([School].self, from: data)

        return schools
    }
}

extension URLSession: URLSessionProtocol {}
