//
//  ErrorHandler.swift
//  NYCSchools
//
//  Created by  Eugene Mammie on 11/14/23.
//

import Foundation

struct NYCSchoolsErrorHandler {
    /*
     example of Centrelized Error Handler
     used to log and handle Error propogation
     */
    static let `default` = NYCSchoolsErrorHandler()

    let genericMessage = "Sorry! Something went wrong"

    func logError(error _: Error) {
        presentError(errorMessage: genericMessage)
    }

    func logError(error: LocalizedError) {
        if let errorDescription = error.errorDescription {
            presentError(errorMessage: errorDescription)
        } else {
            presentError(errorMessage: genericMessage)
        }
    }

    func presentError(errorMessage: String) {
        print(errorMessage)
    }
}
