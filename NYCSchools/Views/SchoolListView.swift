//
//  SchoolListView.swift
//  NYCSchools
//
//  Created by  Eugene Mammie on 11/13/23.
//

import SwiftUI

struct SchoolListView: View {
    @ObservedObject var viewModel: SchoolListViewModel

    var body: some View {
        NavigationStack {
            List {
                ForEach(viewModel.schools) { school in
                    SchoolListCell(school: school)
                }
            }
            .navigationTitle("🗽 NYCSchools")
            .task {
                let task = viewModel.refreshSchools()

                if let schools = try? await task.value {
                    viewModel.schools = schools
                }
            }
            .onAppear(perform: {
                refresh()
            })
            .refreshable {
                // Handles a pull to refresh
                refresh()
            }
            .navigationDestination(for: School.self) { school in
                SchoolDetailView(school: school)
            }
        }
    }

    func refresh() {
        _ = viewModel.refreshSchools()
    }
}

struct SchoolListCell: View {
    @State var school: School
    var body: some View {
        HStack {
            Text("🏫")
            NavigationLink(school.name, value: school)
        }
    }
}

#Preview {
    let viewModel = SchoolListViewModel()

    return
        SchoolListView(viewModel: viewModel)
}

#Preview("Dark Theme") {
    let viewModel = SchoolListViewModel()

    return SchoolListView(viewModel: viewModel)
        .environment(\.colorScheme, .dark)
}
