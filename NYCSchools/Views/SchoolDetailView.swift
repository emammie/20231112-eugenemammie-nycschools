//
//  SwiftDetailView.swift
//  NYCSchools
//
//  Created by  Eugene Mammie on 11/14/23.
//

import SwiftUI

struct SchoolDetailView: View {
    @State var school: School

    var body: some View {
        ZStack {
            Color("Background")
                .edgesIgnoringSafeArea(/*@START_MENU_TOKEN@*/ .all/*@END_MENU_TOKEN@*/)
            VStack(alignment: .leading) {
                HStack(alignment: .top) {
                    Text("📒 School Name:")
                    Text(school.name)
                }
                HStack {
                    Text("🆔")
                    Text(school.id)
                }
                Text("🚌 Number of Participants: " + (school.numOfParticipantsSAT?.description ?? "N/A"))
                Text("📚 Avg. Critical Thinking Scores :" + (school.critcalReadAvgScoresSAT?.description ?? "N/A"))
                Text("📝 Avg. Writing Scores : " + (school.writingAvgScoresSAT?.description ?? "N/A"))
                Text("🧮 Avg. Math Scores :" + (school.mathAvgScoresSAT?.description ?? "N/A"))
            }
        }
        .navigationTitle(Text(school.name))
    }
}

#Preview {
    let schools = [School(id: "School of arts ", name: "School of arts", numOfParticipantsSAT: "54")]

    return SchoolDetailView(school: schools[0])
}

#Preview("Dark Theme") {
    let schools = [School(id: "School of arts ", name: "School of arts", numOfParticipantsSAT: "54")]

    return SchoolDetailView(school: schools[0])
        .environment(\.colorScheme, .dark)
}
