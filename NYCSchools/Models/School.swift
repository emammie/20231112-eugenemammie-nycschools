//
//  School.swift
//  NYCSchools
//
//  Created by  Eugene Mammie on 11/13/23.
//

import Foundation

struct School: Codable, Identifiable, Hashable {
    enum CodingKeys: String, CodingKey {
        case id = "dbn"
        case name = "school_name"
        case boro
        case overview = "overview_paragraph"
        case schoolSeats = "school_10th_seats"
        case academics = "academicopportunities1"

        case numOfParticipantsSAT = "num_of_sat_test_takers"
        case critcalReadAvgScoresSAT = "sat_critical_reading_avg_score"
        case mathAvgScoresSAT = "sat_math_avg_score"
        case writingAvgScoresSAT = "sat_writing_avg_score"
    }

    var id: String
    var name: String
    var boro: String?
    var overview: String?
    var schoolSeats: String?
    var academics: String?

    var numOfParticipantsSAT: String?
    var critcalReadAvgScoresSAT: String?
    var mathAvgScoresSAT: String?
    var writingAvgScoresSAT: String?
}
