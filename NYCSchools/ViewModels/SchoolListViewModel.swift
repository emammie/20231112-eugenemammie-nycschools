//
//  SchoolListViewModel.swift
//  NYCSchools
//
//  Created by  Eugene Mammie on 11/13/23.
//

import Foundation

@MainActor /* Necessary for Main Thread Tasks*/
class SchoolListViewModel: ObservableObject {
    @Published var schools: [School]
    var loader: LoadingClient

    init(_ loaderClient: LoadingClient = NYCSchoolRemoteClient()) {
        schools = [School]()
        loader = loaderClient
    }

    func refreshSchools() -> Task<[School], Error> {
        return Task {
            guard let schools = try? await loader.fetchSchools() else {
                throw LoadingError.clientLoadingError
            }
            return schools
        }
    }
}
