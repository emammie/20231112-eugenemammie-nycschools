//
//  NYCSchoolsApp.swift
//  NYCSchools
//
//  Created by  Eugene Mammie on 11/13/23.
//

import SwiftUI

@main
struct NYCSchoolsApp: App {
    var body: some Scene {
        WindowGroup {
            let viewModel = SchoolListViewModel()
            SchoolListView(viewModel: viewModel)
        }
    }
}
