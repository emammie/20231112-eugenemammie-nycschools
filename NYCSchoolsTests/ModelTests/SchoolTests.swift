//
//  SchoolTests.swift
//  NYCSchoolsTests
//
//  Created by  Eugene Mammie on 11/13/23.
//

import XCTest
@testable import NYCSchools

final class SchoolTests: XCTestCase {
    var sut: School!
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testCreateSchoolName(){
        sut = School(id: "XXXX", name: "High School of the Arts")
        XCTAssertNotNil(sut.name)
        XCTAssert(sut.name == "High School of the Arts")
    }
    
    func testCreateSchoolsFromJSON(){
        if let path = Bundle.main.path(forResource: "test", ofType: "json")
        {
            do {
                let jsonData = try Data(contentsOf: URL(fileURLWithPath: path))
                let decoder = JSONDecoder()
                let listOfSchools = try! decoder.decode([School].self, from: jsonData)
                
                XCTAssertNotNil(listOfSchools)
                XCTAssert(listOfSchools.count > 1)
            }
            catch {
                print("Problem Parsing")
            }
        } else {
            print("Target missing test file")
        }
    }
    
    func testParseJSON(){
        if let path = Bundle.main.path(forResource: "schooldata", ofType: "json")
        {
            do {
                let jsonData = try Data(contentsOf: URL(fileURLWithPath: path))
                let decoder = JSONDecoder()
                let listOfSchools = try! decoder.decode([School].self, from: jsonData)
            
                XCTAssertNotNil(listOfSchools)
                XCTAssert(listOfSchools.count > 1)
            }
            catch {
                print("Problem Parsing")
            }
        } else {
            print("Target missing test file")
        }
    }
}

