//
//  NYCRemoteClientTests.swift
//  NYCSchoolsTests
//
//  Created by  Eugene Mammie on 11/13/23.
//

import XCTest
@testable import NYCSchools

final class NYCRemoteClientTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testClientFetchesSchools() async throws{
        let sut = NYCSchoolRemoteClient()
        let schools = try await sut.fetchSchools()
       
        XCTAssertNotNil(schools)
        XCTAssert(schools.count > 1)
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
