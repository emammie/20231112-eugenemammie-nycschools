//
//  SchoolListViewModelTests.swift
//  NYCSchoolsTests
//
//  Created by  Eugene Mammie on 11/13/23.
//

import XCTest
@testable import NYCSchools
@MainActor
final class SchoolListViewModelTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testInitViewModel() throws {

        let viewModel = SchoolListViewModel()
        
        XCTAssertNotNil(viewModel)
    
        XCTAssertNotNil(viewModel.loader)
        XCTAssert(viewModel.schools.count == 0 )
    }
    
    func testFetchOfViewModel() async throws {
        let sut = SchoolListViewModel()
        let task = sut.refreshSchools()
        let schools = try await task.value
        
        XCTAssertTrue(schools.count > 0)
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
