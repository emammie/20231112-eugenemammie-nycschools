# NYCSchools Documentation

JP Morgan Take-Home iOS Exercise

## Project

A barebone iOS app example using modern Frameworks of SwiftUI and Async/Await to fetch data about NYC Schools SAT Scores

## Building
open using XCode 15.0 or greater


## Notes

### Concepts Implemented
* **SwiftUI**
* **Async/Await**
* **View/ViewModel**
* **Unit Testing**
* **Dependency Injectiono**
* **Protocol Oriented Programming**
* **Error Handling**

### Reasoning

This project could be built a multitude of different ways including with UIKit and with Objective-C 
I chose to use SwiftUI in combination with Async/Await patterns to reflect modern iOS Paradigms
With these patterns in mind it determined how the unit tests were written as well

The UI is a simple use of NavigationStack and Stack Views to display school info similar 
to how tableView/CollectionView might have been used in the past 
SchoolListView displays Schools and on tap display a "Detail" view 
With SwiftUI the preview provider macros serve as static UI references acting almost as UI tests 
SwiftUI provides many observation pattern tools such as the ObservableObject label for the UI to be updated as soon as underlying data changes 
This lends itself to the viewModel pattern which also has its own unit tests 

Fetching data was done in the form a remote Client which used swift's async/await patterns as opposed to more traditional GranDispatch Queues call data asyncrhoounsly and then update it later on the main queue
 
With more time These considerations
* Separate Coordinator to control creation of Detail Views and Navigation when more screens are added
* Building URL Requests over static input of the urls
* Prefetching & Caching for more data
* Extensive Unit Testing on components
* UI Tests for Intergration
* Linting and Formating added to build Phases
